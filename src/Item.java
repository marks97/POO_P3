/*
 * Authors: Rupesh Pons u137564 i Marc Amorós u138095
 *
 * Aquesta és la classe Item. En el constructor rebràs tots els atributs que han estat passats a la classe com a paràmetres.
 *
 * Com a atributs tenim un String name, un String type on hi posem el tipus d'Item que és, un vector de doubles anomenat size,
 * en el qual posem les mides de l'item. I un double anomenat cost, el qual contindrà el cost de l'item.

*/

import java.util.Comparator;
import java.util.LinkedList;

public abstract class Item {

    private String name;
    private String type;

    private double[] size;
    private double cost;

    private Package pack;

    public Item (String name, String type, double[] size, double cost){
        this.name = name;
        this.type = type;
        this.size = size;
        this.cost = cost;
    }

    public String getName(){
        return name;
    }

    public String getType(){
        return type;
    }

    public double[] getSize(){
        return size;
    }

    public double getCost() {
        return cost;
    }

    public Package getPackage() {
        return pack;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setSize(double[] size) {
        this.size = size;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public boolean assingBestPackage(LinkedList<Package> ip){

        //Ordenem la llista de paquets de petit a gran per que al
        // cercar-ne un que s'adapti a l'item sigui el més petit possible
        ip.sort(Comparator.comparing(Package::getHeight));

        int []sizeInt = new int[size.length];
        boolean needBox = false;

        for (int x = 0; x<size.length; x++){    //per fer comparacions pasem els doubles de size a ints
            sizeInt[x] = (int)size[x];
        }

        if (size[2]<3){    //cas de que sigui un envelope
            for (Package p : ip){   //bucle per passar per cada un de les posicions de la Linkedlist ip
                if (p instanceof Envelope){ //mira si la posició en la que ens trobem es un envelope
                    Envelope penvelope = (Envelope) p;  //utilitzem el downcast
                    if (penvelope.isSuitable(sizeInt)) {
                        this.pack = p;
                        ip.remove(p);
                        break;
                    }  //mira si el paquet i l'Item son compatibles, en cas afirmatiu l'assigna com lenvelope de litem
                }
            }
            needBox = true;
        }

        if(needBox) {    //en el cas de que depth sigui mes gran o igual sera un box, un envelope altrement
            for (Package p : ip){   //bucle per passar per cada un de les posicions de la Linkedlist ip
                if (p instanceof Box){  //per mirar si la posicio que ens trobem de la linked list es un box
                    Box pbox = (Box) p; //ara que sabem que el paquet es de tipus box creem un box per utilitzar el metode isSuitable
                    if(pbox.isSuitable(sizeInt)){
                        this.pack = p;
                        ip.remove(p);
                        return true;
                    }    //mira si el paquet i litem son compatibles, en cas afirmatiu lassigna com el paquet de litem
                }
            }
        }

        return false;

    }

    public abstract double getPrice();

    public abstract  double calculateProfit();

}
