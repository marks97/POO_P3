/*
 * Authors: Rupesh Pons u137564 i Marc Amorós u138095
 *
 * Aquesta és la classe User.
 *
 * Com a atributs tenim un String name, un String id, on hi posem el tipus l'identificador de l'usuari, i un
 * string pass que conté la contrasenya d'aquest usuari.

*/

public class User {

    private String username;
    private String identifier;
    private String password;

    public User(String name, String id, String pass) {
        username = name;
        identifier = id;
        password = pass;
    }

    public String getUsername() {
        return username;
    }

    public String getIdentifier() {
        return identifier;
    }

    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean login(String password) {
        return
                password.equals(this.password);
    }

}
