/*

* @authors: Rupesh Pons u137564 i Marc Amorós u138095
*
* Aquesta és la classe principal del programa. Conté les funcions necessàries per al funcionament del programa.
*
*/

import java.util.LinkedList;
import java.util.Scanner;

public class OnlineStore {

    /*
     * Aquestes constants defineixen els magic numbers dels menús i les opcions.
    */

    private static final int CLOSE_PROGRAM = 0;

    private static final int MENU_LOGIN = 1;
    private static final int MENU_SELLER = 2;
    private static final int MENU_BUYER = 3;
    private static final int MENU_ADMIN = 4;

    private static final int LOGIN = 1;

    private static final int SELL_ITEM = 1;
    private static final int VIEW_ITEMS = 2;
    private static final int LOGOUT_SELLER = 3;

    private static final int BUY_ITEM = 1;
    private static final int VIEW_BOUGHT_ITEMS = 2;
    private static final int DEPOSIT = 3;
    private static final int LOGOUT_BUYER = 4;

    private static final int EXPEL_USER = 1;
    private static final int LOGOUT_ADMIN = 2;


    /*
    * Aquests vectors formen la base de dades del programa, formada per els seus
    * usuaris, els elements de la botiga (disponibles i venuts) i els paquets.
    */

    private LinkedList<User> users = new LinkedList<>();
    private LinkedList<Item> itemsAvailable = new LinkedList<>();
    private LinkedList<Item> itemsSold = new LinkedList<>();
    private LinkedList<Package> packages = new LinkedList<>();

    private Scanner sc = new Scanner(System.in);
    private Integer CURRENT_USER = -1;

    private Bank bank = new Bank(); //Aquest objecte conté totes els comptes bancaris dels usuaris, i els seus diners.

    private double totalPrice = 92026.98, totalProfit = 12210.0;


    /*
    * Aquesta és la funció principal del programa. Omplirem la DB i iniciarem el bucle
    * que s'encarrega de la continuïtat del menú
    */

    public static void main(String[] args) {

        OnlineStore os = new OnlineStore();
        int menu = 1;

        os.fillFakeDB();

        while (menu!=CLOSE_PROGRAM) {
            os.showMenu(menu);
            System.out.println("\nType an option: ");
            menu = os.selectOption(menu);
        }

    }

    /*
    * Aquesta funció podriem dir que és de tipus int, però també fa la funció de boolean.
    * Si l'usuari que busquem (per la seva id) no existeix retorna un -1. Si l'usuari es
    * troba a la DB, retornem la seva posició.
    */

    private int existsUser(String u) {

        for(int x = 0; x<users.size(); x++) {
            if(users.get(x).getIdentifier().equals(u))
                return x;
        }

        return -1;
    }

    /*
    * Aquesta funció s'encarrega de, en funció de l'enter que rep com a paràmetre, imprimir
    * per pantalla un menú o un altre.
    */

    private void showMenu(int menu) {

        System.out.println("\nTotal price " + totalPrice + " €  Total profit " + totalProfit + " €");

        if(menu == MENU_LOGIN) {
            System.out.println("\n\n[0] Close App |  [1] Log In");
        } else if(menu == MENU_SELLER) {
            System.out.println("\n\n[0] Close App |  [1] Add item  | [2] My sells  | [3] Log out");
        } else if(menu == MENU_BUYER) {
            System.out.println("\n\n[0] Close App |  [1] Buy item  | [2] My purchases  | [3] Deposit  | [4] Log out");
        } else  if (menu == MENU_ADMIN) {
            System.out.println("\n\n[0] Close App |  [1] Expel user  | [2] Manage auctions  | [3] Log out");
        }
    }

    /*
    * Aquesta funció s'encarrega de, en funció de l'opció seleccionada per l'usuari, executar una
    * funció en concret.
    *
    * Si l'enter 'menu' és igual a 1, siginifica que el menú impres ha set el numero 1, amb les opcions
    * corresponents al log in.
    *
    *      "      "     "  igual a 2, el menú impres conté les opcions corresponents a les d'un venedor.
    *
    *      "      "     "  igual a 3, el menú impres conté les opcions corresponents a les d'un comprador.
    *
    *      "      "     "  igual a 4, el menú impres conté les opcions corresponents a les d'un administrador.
    *
    */

    private int selectOption(int menu) {

        int select = Integer.parseInt(sc.nextLine());

        switch (menu) {

            case MENU_LOGIN: {

                //Aquesta opció permet a l'usuari loguejar-se, o tencar l'aplicació.

                switch (select) {

                    case LOGIN: {

                        System.out.println("\n\nType your identifier");
                        String id = sc.nextLine();
                        int x = existsUser(id); //Comprobem que existeix l'usuari.

                        if (x > 0) { //Si existeix demanem la contrasenya

                            User u = users.get(x);
                            System.out.println("\nType your password");
                            String password = sc.nextLine();

                            if (u.login(password)) {
                                //Quan el login és correcte, retornem el menú següent (en funció del tipus d'usuari)
                                System.out.println("\nCorrect log in. Hello " + u.getUsername());
                                CURRENT_USER = x;

                                if (u instanceof Seller)
                                    return MENU_SELLER;

                                else if (u instanceof Buyer)
                                    return MENU_BUYER;

                                else
                                    return MENU_ADMIN;

                            } else {
                                System.out.println("\nIncorrect password");
                                return MENU_LOGIN;
                            }

                        } else {
                            System.out.println("\nUser " + id + " is not on our DB, sorry.");
                            return MENU_LOGIN;
                        }
                    }

                    case CLOSE_PROGRAM:
                        break;

                    default: {
                        System.out.println("\n\nInvalid option");
                        return MENU_LOGIN;
                    }

                }

                break;

            }

            case MENU_SELLER: { // Menú d'un venedor

                Seller user = (Seller) users.get(CURRENT_USER);
                System.out.println("\nMoney available: " + user.getMoney(bank));

                switch (select) {

                    case SELL_ITEM : {

                        //Aquesta opció permet a l'usuari publicar un Item a la botiga. Se li demanen totes les dades
                        //de l'objecte.

                        AuctionItem item = new AuctionItem("", "", new double[3],
                                5, 0, "31-12-2017 23:59:59", user);

                        System.out.println("\n\nType item name: ");
                        item.setName(sc.nextLine());

                        System.out.println("\nType item type: ");
                        item.setType(sc.nextLine());

                        System.out.println("\nType item width (in cm)");
                        double w = Double.parseDouble(sc.nextLine());

                        System.out.println("\nType item height (in cm)");
                        double h = Double.parseDouble(sc.nextLine());

                        System.out.println("\nType item depth (in cm)");
                        double d = Double.parseDouble(sc.nextLine());
                        item.setSize(new double[]{w, h, d});

                        System.out.println("\nType item price (in €)");
                        item.setPrice(Double.parseDouble(sc.nextLine()));

                        System.out.println("\nType item deadLine. IMPORTANT follow this example format: 31-12-2017 23:59:59");
                        item.setDeadLine(sc.nextLine());

                        user.addAvailableItem(item);
                        itemsAvailable.add(item);
                        totalPrice += item.getPrice();

                        return MENU_SELLER;

                    }

                    case VIEW_ITEMS: { //Aquesta opció permet a l'usuari veure quins Items té publicats a la venda.

                        System.out.println("\n\nThis is your available items:");

                        for (AuctionItem i : user.getAvailableItems()) {
                            System.out.println("\n - " + i.getName() + " (" + i.getType() + ") .  [" + i.getPrice() + " € ]");
                        }

                        System.out.println("\n\nThis is your sold items:");

                        for (AuctionItem i : user.getSoldItems()) {
                            System.out.println("\n - " + i.getName() + " (" + i.getType() + ") .  [" + i.getPrice() + " € ]");
                        }

                        return MENU_SELLER;

                    }

                    case LOGOUT_SELLER: {

                        System.out.println("\n\nLogged out."); //En aquest cas l'usuari tanca la sessió oberta.
                        CURRENT_USER = -1;

                        return MENU_LOGIN;
                    }

                    case CLOSE_PROGRAM:
                        break;

                    default: {
                        System.out.println("\n\nInvalid option");
                        return MENU_LOGIN;
                    }
                }

                break;

            }

            case MENU_BUYER: { //Menú d'un comprador

                Buyer user = (Buyer) users.get(CURRENT_USER);
                System.out.println("\nMoney available: " + user.getMoney(bank));

                switch (select) {

                    case BUY_ITEM: {

                        //Aquesta opció permet a l'usuari comprar Items. Primer seràn tots llistats per pantalla.

                        System.out.println("\n\nThis is the actual stock");

                        for (int x = 0; x < itemsAvailable.size(); x++) {

                            Item i = itemsAvailable.get(x);

                            if (i instanceof AuctionItem)
                                System.out.println("\n[" + x + "] - " + i.getName() + " (" + i.getType() + ") .  " +
                                        "[" + i.getPrice() + " € ]");

                            else if (i instanceof UnitItem)
                                System.out.println("\n[" + x + "] - " + i.getName() + " (" + i.getType() + ") .  " +
                                        "[" + i.getPrice() + " € / unit ],  --> " + ((UnitItem) i).getQuantity());

                            else
                                System.out.println("\n[" + x + "] - " + i.getName() + " (" + i.getType() + ") .  " +
                                        "[" + i.getPrice() + " € / kg ],  --> " + ((WeightedItem) i).getWeight());

                        }

                        System.out.println("\n\nSelect one item: "); //L'usuari indica l'Item que vol comprar
                        int index = Integer.parseInt(sc.nextLine());
                        Item item = itemsAvailable.get(index);

                        if (user.buy(item, bank)) { //Comprovem que l'usuari es pot permetre la compra

                            //En funció de quin tipus d'Item sigui, executarem unes instruccions o unes altres.

                            if (item instanceof AuctionItem) {
                                ((AuctionItem) item).getSeller().sell((AuctionItem) item, bank);
                                itemsAvailable.remove(item);
                                itemsSold.add(item);
                                totalProfit += 5;
                                totalPrice -= item.getPrice();
                                System.out.println("\nThe bought has been completed. Congratulations!");

                            } else if (item instanceof UnitItem) {

                                System.out.println("\nType how many quantity of the item do you want: ");
                                int q = Integer.parseInt(sc.nextLine());
                                if (q <= ((UnitItem) item).getQuantity()) {
                                    totalPrice -= ((UnitItem) item).sell(q);
                                    totalProfit += item.calculateProfit();
                                    ((UnitItem) item).setQuantity(((UnitItem) item).getQuantity() - q);

                                    if (((UnitItem) item).getQuantity() == 0)
                                        itemsAvailable.remove(item);
                                    else
                                        itemsAvailable.set(index, item);

                                    if (itemsSold.contains(item))
                                        itemsSold.set(itemsSold.indexOf(item), item);
                                    else
                                        itemsSold.add(item);

                                    System.out.println("\nThe bought has been completed. Congratulations!");

                                } else
                                    System.out.println("\nThere aren't too much " + item.getName());

                            } else {

                                System.out.println("\nType how many quantity of the item do you want (in kg): ");
                                double q = Double.parseDouble(sc.nextLine());
                                if (q <= ((WeightedItem) item).getWeight()) {
                                    totalPrice -= ((WeightedItem) item).sell(q);
                                    totalProfit += item.calculateProfit();
                                    ((WeightedItem) item).setWeight(((WeightedItem) item).getWeight() - q);

                                    if (((WeightedItem) item).getWeight() == 0)
                                        itemsAvailable.remove(item);
                                    else
                                        itemsAvailable.set(index, item);

                                    if (itemsSold.contains(item))
                                        itemsSold.set(itemsSold.indexOf(item), item);
                                    else
                                        itemsSold.add(item);

                                    System.out.println("\nThe bought has been completed. Congratulations!");

                                } else
                                    System.out.println("\nThere aren't too much " + item.getName());
                            }

                            if (!item.assingBestPackage(packages)) { //En cas que no hi hagi cap paquet disponible per l'Item, el preu pujarà
                                System.out.println("\nWe cannot assign one of our packages. The construction of a new package has a cost of 2 eur");
                                totalProfit += 1;
                            }

                        } else { //Si l'usuari no es pot permetre comprar l'objecte
                            System.out.println("\nYou cannot buy this item");
                        }

                        return MENU_BUYER;

                    }

                    case VIEW_BOUGHT_ITEMS: { //Aquesta opció permet a l'usuari veure quins Items ha comprat

                        System.out.println("\n\nThis is your items purchased");

                        for (Item i : user.getBoughtItems()) {
                            System.out.println("\n- " + i.getName() + " (" + i.getType() + ") .  [" + i.getPrice() + " € ]");
                        }

                        return MENU_BUYER;

                    }

                    case DEPOSIT: { //Aquesta opció permet a l'usuari posar diners al seu compte

                        System.out.println("\nHow much money you want to deposit?");
                        int money = Integer.parseInt(sc.nextLine());
                        if (money > 0) {
                            user.deposit(money, bank);
                        }

                        return MENU_BUYER;

                    }

                    case LOGOUT_BUYER: {

                        System.out.println("\n\nLogged out.");
                        CURRENT_USER = -1;
                        return MENU_LOGIN;

                    }

                    case CLOSE_PROGRAM:
                        break;

                    default: {
                        System.out.println("\n\nInvalid option");
                        return MENU_LOGIN;
                    }
                }

                break;

            }

            case MENU_ADMIN: { //Aquest és el menú de l'usuari de tipus administrador

                Administrator admin = (Administrator) users.get(CURRENT_USER);

                switch (select) {

                    case EXPEL_USER: { //Aquesta opció permet a l'administrador expulsar a un usuari de la botiga

                        System.out.println("\n\nThis is the users list");
                        for (int i = 0; i < users.size(); i++) {
                            User u = users.get(i);
                            System.out.println("\n[" + i + "] - " + u.getUsername() + " id = " + u.getIdentifier());
                        }

                        System.out.println("\n\nSelect which user you want to expel: ");
                        User user = users.get(Integer.parseInt(sc.nextLine()));

                        if (admin.expel(user, users)) {
                            System.out.println("\nUser " + user.getUsername() + " deleted");
                        } else
                            System.out.println("\nCannot delete user.");

                        return MENU_ADMIN;

                    }

                    case LOGOUT_ADMIN: {

                        System.out.println("\n\nLogged out.");
                        CURRENT_USER = -1;

                        return MENU_LOGIN;
                    }

                    case CLOSE_PROGRAM:
                        break;

                    default: {
                        System.out.println("\n\nInvalid option");
                        return MENU_LOGIN;
                    }
                }

               break;

            }
        }

        return CLOSE_PROGRAM;
    }

    /*
    * Aquesta funció omple una base de dades falsa, amb usuaris, items i paquets inventats, i realitza també algunes transaccions
    * ficticies.
    */

    private void fillFakeDB() {

        Seller seller1 = new Seller("Sergi" , "s.besses", "sSA¿ksk!2", "ES12 1231 6349 10 6737856120");
        Seller seller2 = new Seller("Gemma" , "belmont98", "pass123", "ES16 6535 8763 19 4573278548");
        Seller seller3 = new Seller("Eloi" , "eloi.codina", "ñkoij12RRe?", "ES19 4522 9526 35 9643272851");

        users.add(new Buyer("Rupesh" , "r.pons", "passWord1234", "ES12 0032 5467 32 4726732343"));
        users.add(new Buyer("Marc" , "marks97", "pass321" , "ES22 2331 6539 12 0236876230"));
        users.add(new Buyer("Marina" , "marina99", "38483M?!", "ES32 7536 9742 34 5632782301"));
        users.add(new Buyer("Anna" , "anna12", "asqawed12" , "ES11 6510 1830 12 8319745091"));
        users.add(new Buyer("Nate" , "nate94", "sdfsasd34?", "ES12 2386 1598 12 3486542349"));
        users.add(seller1);
        users.add(seller2);
        users.add(seller3);
        users.add(new Administrator("Pol" , "pol.beat12", "sadfawe34!"));
        users.add(new Administrator("Víctor" , "vpiella28", "sjcvksd4Xs?"));


        itemsAvailable.add(new AuctionItem("Mustang ", "Zapatillas", new double[]{30,30,40}, 5, 29.99, "29-11-2017 22:00:00", seller1));
        itemsAvailable.add(new AuctionItem("Mac mini 2011", "Informática", new double[]{30,30,40}, 5, 300, "25-11-2017 18:30:00", seller2));
        itemsAvailable.add(new AuctionItem("Marshall ", "Altavoces", new double[]{40,20,20}, 5, 99.99, "26-12-2017 12:15:00", seller2));
        itemsAvailable.add(new UnitItem("Silla gamer ", "Sillas", new double[]{100,40,100}, 28.99, 29.99, 100));
        itemsAvailable.add(new UnitItem("Naruto", "Comic", new double[]{5,10,2}, 7.00, 7.99, 200));
        itemsAvailable.add(new UnitItem("Oxford", "Libreta", new double[]{21,23,2}, 9.99, 10, 100));
        itemsAvailable.add(new UnitItem("i3 Intel", "Informática", new double[]{3,3,1}, 150, 200, 300));
        itemsAvailable.add(new WeightedItem("Alpiste ", "Comida", new double[]{50,50,100}, 3, 5, 2000));
        itemsAvailable.add(new WeightedItem("Manzana ", "Comida", new double[]{50,50,100}, 3, 8, 2000));


        itemsSold.add(new UnitItem("i3 Intel", "Informática", new double[]{3,3,1}, 150, 200, 200));
        itemsSold.add(new UnitItem("Silla gamer ", "Sillas", new double[]{100,40,100}, 25.99, 29.99, 50));
        itemsSold.add(new AuctionItem("Renault Clio", "Coche", new double[]{300,200,500}, 5, 950, "29-11-2017 22:00:00", seller1));
        itemsSold.add(new AuctionItem("JVC ", "Altavoces", new double[]{10,20,10}, 5, 9.99, "22-11-2017 22:00:00", seller3));
        itemsSold.add(new WeightedItem("Banana", "Comida", new double[]{30,50,50}, 2, 4, 1000));

        seller1.addAvailableItem((AuctionItem)itemsSold.get(2));
        seller1.addAvailableItem((AuctionItem)itemsAvailable.get(0));
        seller1.sell(seller1.getAvailableItems().get(0) , bank);

        seller2.addAvailableItem((AuctionItem)itemsAvailable.get(1));
        seller2.addAvailableItem((AuctionItem)itemsAvailable.get(2));

        seller3.addAvailableItem((AuctionItem)itemsSold.get(3));
        seller3.sell(seller3.getAvailableItems().get(0) , bank);

        packages.add(new Box(40,50,50));
        packages.add(new Box(40,50,50));
        packages.add(new Box(20,30,30));
        packages.add(new Box(40,50,50));
        packages.add(new Box(20,30,100));
        packages.add(new Box(20,10,15));
        packages.add(new Box(100,40,100));
        packages.add(new Box(50,50,50));
        packages.add(new Box(10,10,40));
        packages.add(new Box(300,300,500));
        packages.add(new Envelope(21, 29, "A4"));
        packages.add(new Envelope(21, 11, "A5"));
        packages.add(new Envelope(21, 29, "A4"));
        packages.add(new Envelope(29, 42, "A3"));
        packages.add(new Envelope(21, 11, "A5"));
        packages.add(new Envelope(29, 42, "A3"));
        packages.add(new Envelope(21, 29, "A4"));
        packages.add(new Envelope(21, 11, "A5"));
        packages.add(new Envelope(29, 42, "A3"));

    }

}