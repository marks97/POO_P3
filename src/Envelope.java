/*
 * Authors: Rupesh Pons u137564 i Marc Amorós u138095
 *
 * Aquesta és la subclasse Envelope, la qual és filla de la classe Package. Aquesta classe compte amb un constructor,
 * el qual li passem per paràmetres els atributs de la seva classe pare i l'atribut d'aquesta.
 *
 * Aquesta subclasse compte amb un sol atribut, el qual és un String name i conté el nom del Envelope.
 *
*/

public class Envelope extends Package {

    private String name;

    public Envelope(int width, int height, String name) {
        super(width,height);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSuitable(int[] size) {
        return (size[0]<=width && size[1]<=height);
    }
}
