/*
 * Authors: Rupesh Pons u137564 i Marc Amorós u138095
 *
 * Aquesta és la classe Package. En el constructor li passem per paràmetres els atributs d'aquesta pròpia classe
 *
 * Aquesta classe té dos atributs: un int witdth el qual conté la amplada del paquet, i un int height el qual conté la alçada
 *
*/

public class Package {

    protected int width;
    protected int height;

    public Package(int width, int height) {
        this.width = width;
        this.height = height;
    }
    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void setWidth(int width){
        this.width = width;
    }

    public void setHeight(int height){
        this.height = height;
    }
}

