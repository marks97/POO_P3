import java.util.LinkedList;

public class Administrator extends User {

    public Administrator(String name, String id, String pass) {
        super(name, id, pass);
    }

    public boolean expel(User user, LinkedList<User> users) {
        if(users.contains(user) && !(user instanceof Administrator)) {
            users.remove(user);
            return true;
        } else
            return false;
    }

    public boolean manageAuction(AuctionItem a, String date) {
        return true;
    }

    private void printStock(LinkedList<Item> items) {

        System.out.println("\nThis are the stock items\n");
        for(Item item : items) {
            if(item instanceof AuctionItem)
                System.out.println(" - " + item.getName() + "   [" + item.getPrice() + "€]\n");
        }
    }

}
