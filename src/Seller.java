import java.util.LinkedList;

public class Seller extends User {

    private String accountNumber;
    private LinkedList<AuctionItem> soldItems, availableItems;

    public Seller(String name, String id, String pass, String accountNumber) {
        super(name, id, pass);
        this.accountNumber = accountNumber;
        soldItems = new LinkedList<>();
        availableItems = new LinkedList<>();
    }

    public LinkedList<AuctionItem> getAvailableItems() {
        return availableItems;
    }

    public LinkedList<AuctionItem> getSoldItems() {
        return soldItems;
    }

    public void sell(AuctionItem item, Bank b) {

        double price = item.getPrice();
        if(deposit(price, b)) {
            soldItems.add(item);
            availableItems.remove(item);
        }
    }

    public void addAvailableItem(AuctionItem item) {
        availableItems.add(item);
    }

    private boolean deposit(double price, Bank b) {
        return
                b.manageMoney(accountNumber, price);
    }

    public double getMoney(Bank bank) {
        return bank.getMoney(accountNumber);
    }

}
