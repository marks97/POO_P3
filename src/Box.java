/*
 * Authors: Rupesh Pons u137564 i Marc Amorós u138095
 *
 * Aquesta és la subclasse Box, la qual és filla de la classe Package. Aquesta classe compte amb un constructor,
 * el qual li passem per paràmetres els atributs de la seva classe pare i l'atribut d'aquesta.
 *
 * Aquesta subclasse compte amb un sol atribut, el qual és un int depth i conté la profunditat del paquet.
 *
*/

public class Box extends Package {

    private int depth;

    public Box(int width, int height, int depth){
        super(width,height);
        this.depth = depth;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public boolean isSuitable(int[] size){
        return (size[0]<=width && size[1]<=height && size[2]<=depth);
    }

}
