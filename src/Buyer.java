import java.util.LinkedList;

public class Buyer extends User {

    private String accountNumber;
    private LinkedList<Item> boughtItems;

    public Buyer(String name, String id, String pass, String accountNumber) {
        super(name, id, pass);
        this.accountNumber = accountNumber;
        boughtItems = new LinkedList<>();
    }

    public boolean buy(Item item, Bank b) {

        double price = item.getPrice();
        if(pay(price * (-1), b)) {
            boughtItems.add(item);
            return true;
        }
        else {
            return false;
        }
    }

    public LinkedList<Item> getBoughtItems() {
        return boughtItems;
    }

    private boolean pay(double price, Bank b) {
        return
                b.manageMoney(accountNumber, price);
    }

    public void deposit(double m, Bank b) {
        b.manageMoney(accountNumber, m);
    }

    public double getMoney(Bank bank) {
        return bank.getMoney(accountNumber);
    }
}
